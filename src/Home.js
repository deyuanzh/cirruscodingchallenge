import React from 'react';
import TableComponent from './TableComponent';
import axios from 'axios';

class Home extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      cars: []
    };

    this.getAllCars = this.getAllCars.bind(this);
  }

  /*
  Load data from the json
  */
  getAllCars() {
    axios.get('./MOCK_DATA.json')
      .then( response => {
        this.setState({
          cars: response.data
      })
    });
  }

  componentDidMount() {
    this.getAllCars()
  }


  render() {
    const {cars} = this.state;
    const len = cars.length;

    return(
      <React.Fragment>
        {len > 0 && (
          <TableComponent
            myData={cars}
          />
        )}
      </React.Fragment>
    );
  }

}

export default Home;
