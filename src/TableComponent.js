import React from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { ReactComponent as Favorite } from './favorite.svg';
import PropTypes from 'prop-types';

class TableComponent extends React.Component{

  constructor(props) {
    super(props);

    this.state = { 
      currentlySelected: {}, 
      data: this.props.myData, 
      searchText: '',
      favorites: []
    };

    this.selectToggle = this.selectToggle.bind(this);
    this.setFavorites = this.setFavorites.bind(this)
	}
  
  /*
  Handler to set state for checkboxes
  */
  selectToggle(x) {
    var temp = Object.assign({}, this.state.currentlySelected);
    if (x in temp){
      temp[x] = !temp[x];
    } else {
      temp[x] = true;
    }
		this.setState({
			currentlySelected: temp,
		});
  }
  
  /*
  Load the selected favorites into the state
  */
  setFavorites() {
    let newFavorites = Object.assign({}, this.state.currentlySelected);
    let favoriteKeys = Object.keys(newFavorites)
      .filter(function(x){return newFavorites[x]})
      .map(Number)
    let existingFavorites = this.state.favorites;
    
    let difference = favoriteKeys.filter(x => !existingFavorites.includes(x)).concat(existingFavorites.filter(x => !favoriteKeys.includes(x)));
    this.setState({
      favorites: difference,
      currentlySelected: {}
		});
  }

  render(){

    //the table columns
    const columns = [
      {
        id: 'checkbox',
        accessor: '',
        Cell: ({ original }) => {
          return (
            <input
              type='checkbox'
              className='checkbox'
              checked={this.state.currentlySelected[original.id] === true}
              onChange={() => this.selectToggle(original.id)}
            />
          );
        },
        Header: () => {
          return (
            <button onClick = {this.setFavorites}>
              Favorite
            </button>
          )
        },
        sortable: false,
        width: 65
      },
    {
      accessor: 'make',
      Header: 'Make',
    }, {
      accessor: 'model',
      Header: 'Model',
    }, {
      accessor: 'year',
      Header: 'Year',
    }, {
      accessor: 'package',
      Header: 'Package',
    }, {
      accessor: 'fuel',
      Header: 'Fuel Type',
    }, {
      accessor: 'transmission',
      Header: 'Transmission',
    }, {
      accessor: '',
      Header: 'Favorite',
      sortable: false,
      Cell: ({ original }) => {
        return (
            this.state.favorites.includes(original.id) && (
              <Favorite height = '35%' width = '35%'/>
            )
        );
      }
    }
    ];
    
    let data = this.state.data;

    //simple table wide search
    if (this.state.searchText) {
			data = data.filter(row => {
        return row.make.toLowerCase().includes(this.state.searchText.toLowerCase()) || 
        row.model.toLowerCase().includes(this.state.searchText.toLowerCase()) || 
        String(row.year).toLowerCase().includes(this.state.searchText.toLowerCase()) ||
        row.package.toLowerCase().includes(this.state.searchText.toLowerCase()) ||
        row.fuel.toLowerCase().includes(this.state.searchText.toLowerCase()) || 
        row.transmission.toLowerCase().includes(this.state.searchText.toLowerCase())
			})
    }
    
    return(
      <div>
        Search: <input value={this.state.searchText} onChange={e => this.setState({searchText: e.target.value})}/>
        <ReactTable
          data = {data}
          columns = {columns}
          ref={r => this.reactTable = r}
        />
      </div>
    );
  }
}

TableComponent.propTypes = {
  myData: PropTypes.object
}


export default TableComponent;
